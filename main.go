package main

import (
	. "go-ipl-data-project/src"
)

func main() {

	matches := GetMatchesDataFromCSV()
	deliveries := GetDeliveriesDataFromCSV()

	MatchesPlayedPerSeason(matches)
	CalculateMatchesWonPerTeamPerSeason(matches)
	CalculateExtraRunsConcededPerTeam(matches, deliveries, 2016)
	CalculateTopTenEconomicalBowlers(matches, deliveries, 2015)
	CalculateTeamWinTossAndThenMatch(matches)
	CalculateMostPOTMPerSeason(matches)
}
