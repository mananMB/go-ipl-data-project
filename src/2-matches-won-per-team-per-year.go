package src

import (
	"encoding/json"
	"os"
)

type MatchesWonPerTeamPerSeason struct {
	Team          string         `json:"team"`
	WinsPerSeason map[string]int `json:"wins_per_season"`
}

func CalculateMatchesWonPerTeamPerSeason(matches []Match) {
	var matchesWonPerTeamPerSeasonMap = make(map[string]map[string]int)
	var matchesWonPerTeamPerSeason []MatchesWonPerTeamPerSeason

	for _, match := range matches {
		if match.Winner != "" {
			if _, ok := matchesWonPerTeamPerSeasonMap[match.Winner]; !ok {
				matchesWonPerTeamPerSeasonMap[match.Winner] = make(map[string]int)
			}
			matchesWonPerTeamPerSeasonMap[match.Winner][match.Season] = matchesWonPerTeamPerSeasonMap[match.Winner][match.Season] + 1
		}
	}

	for team, wins := range matchesWonPerTeamPerSeasonMap {
		matchesWonPerTeamPerSeason = append(matchesWonPerTeamPerSeason, MatchesWonPerTeamPerSeason{Team: team, WinsPerSeason: wins})
	}

	file, _ := json.MarshalIndent(matchesWonPerTeamPerSeason, "", " ")
	_ = os.WriteFile("output/2-matches-won-per-team-per-year.json", file, 0644)
}
