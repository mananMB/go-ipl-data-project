package src

import (
	"encoding/json"
	"golang.org/x/exp/slices"
	"os"
	"strconv"
)

type ExtraRunsConcededPerTeam struct {
	Team      string `json:"team"`
	ExtraRuns int    `json:"extra_runs"`
}

func CalculateExtraRunsConcededPerTeam(matches []Match, deliveries []Delivery, year int) {
	var extraRunsConcededPerTeamMap = make(map[string]int)
	var extraRunsConcededPerTeam []ExtraRunsConcededPerTeam
	var idsOfMatchesPlayedInYear = getIdOfMatchesPlayedInYear(matches, year)

	for _, delivery := range deliveries {
		if slices.Contains(idsOfMatchesPlayedInYear, delivery.MatchId) {
			var extraRuns, _ = strconv.Atoi(delivery.ExtraRuns)
			extraRunsConcededPerTeamMap[delivery.BattingTeam] += extraRuns
		}
	}

	for team, extraRuns := range extraRunsConcededPerTeamMap {
		extraRunsConcededPerTeam = append(extraRunsConcededPerTeam, ExtraRunsConcededPerTeam{Team: team, ExtraRuns: extraRuns})
	}

	file, _ := json.MarshalIndent(extraRunsConcededPerTeam, "", " ")
	_ = os.WriteFile("output/3-extra-runs-conceded-per-team.json", file, 0644)
}
