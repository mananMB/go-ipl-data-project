package src

import (
	"encoding/json"
	"os"
)

type MatchesPerSeason struct {
	Year    string `json:"year"`
	Matches int    `json:"matches"`
}

func MatchesPlayedPerSeason(matches []Match) {
	var matchesPerSeasonMap = make(map[string]int)
	var matchesPerSeason []MatchesPerSeason

	for _, match := range matches {
		matchesPerSeasonMap[match.Season] += 1
	}

	for year, matches := range matchesPerSeasonMap {
		matchesPerSeason = append(matchesPerSeason, MatchesPerSeason{Year: year, Matches: matches})
	}

	file, _ := json.MarshalIndent(matchesPerSeason, "", " ")
	_ = os.WriteFile("output/1-matches-played-per-year.json", file, 0644)
}
