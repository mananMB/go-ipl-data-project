package src

import (
	"encoding/json"
	"os"
)

type TeamWinTossAndThenMatch struct {
	Team string `json:"team"`
	Wins int    `json:"wins"`
}

func CalculateTeamWinTossAndThenMatch(matches []Match) {
	var teamWinTossAndThenMatchMap = make(map[string]int)
	var teamWinTossAndThenMatch []TeamWinTossAndThenMatch

	for _, match := range matches {
		if match.TossWinner == match.Winner {
			teamWinTossAndThenMatchMap[match.TossWinner] += 1
		}
	}

	for team, wins := range teamWinTossAndThenMatchMap {
		teamWinTossAndThenMatch = append(teamWinTossAndThenMatch, TeamWinTossAndThenMatch{Team: team, Wins: wins})
	}

	file, _ := json.MarshalIndent(teamWinTossAndThenMatch, "", " ")
	_ = os.WriteFile("output/5-teams-win-toss-and-then-the-match.json", file, 0644)
}
