package src

import (
	"encoding/csv"
	"io"
	"log"
	"os"
)

type Delivery struct {
	MatchId     string
	BattingTeam string
	ExtraRuns   string
	Bowler      string
	Batsman     string
	TotalRuns   string
}

const (
	matchId int = iota
	inning
	battingTeam
	bowlingTeam
	over
	ball
	batsman
	nonStriker
	bowler
	isSuperOver
	wideRuns
	byeRuns
	legbyeRuns
	noballRuns
	penaltyRuns
	batsmanRuns
	extraRuns
	totalRuns
	playerDismissed
	dismissalKind
	fielder
)

func GetDeliveriesDataFromCSV() []Delivery {
	var deliveries []Delivery

	deliveriesFile, err := os.Open("./data/deliveries.csv")
	if err != nil {
		log.Fatal(err)
	}

	deliveriesReader := csv.NewReader(deliveriesFile)

	_, err = deliveriesReader.Read()
	if err != nil {
		log.Fatal(err)
	}

	for {
		row, err := deliveriesReader.Read()
		if err == io.EOF {
			break
		}
		if err != nil {
			log.Fatal(err)
		}
		deliveries = append(deliveries, Delivery{
			MatchId:     row[matchId],
			BattingTeam: row[battingTeam],
			ExtraRuns:   row[extraRuns],
			Batsman:     row[batsman],
			Bowler:      row[bowler],
			TotalRuns:   row[totalRuns]})
	}

	return deliveries
}
