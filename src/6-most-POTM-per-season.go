package src

import (
	"encoding/json"
	"os"
)

type MostPlayerOfTheMatchInSeason struct {
	Season string `json:"season"`
	Player string `json:"player"`
	Wins   int    `json:"wins"`
}

func CalculateMostPOTMPerSeason(matches []Match) {
	var playerOfMatchPerSeason = make(map[string]map[string]int)
	var mostPlayerOfMatchPerSeason = make(map[string]map[string]int)
	var mostPlayerOfTheMatchInSeason []MostPlayerOfTheMatchInSeason
	for _, match := range matches {
		if _, ok := playerOfMatchPerSeason[match.Season]; !ok {
			playerOfMatchPerSeason[match.Season] = make(map[string]int)
		}
		playerOfMatchPerSeason[match.Season][match.PlayerOfMatch] += 1
	}

	for season := range playerOfMatchPerSeason {
		if _, ok := mostPlayerOfMatchPerSeason[season]; !ok {
			mostPlayerOfMatchPerSeason[season] = make(map[string]int)
		}
		maxWins := 0
		for player, wins := range playerOfMatchPerSeason[season] {
			if maxWins < wins {
				mostPlayerOfMatchPerSeason[season] = map[string]int{player: wins}
				maxWins = wins
			}
		}
	}

	for season, stat := range mostPlayerOfMatchPerSeason {
		var player string
		var wins int
		for k, v := range stat {
			player = k
			wins = v
		}
		mostPlayerOfTheMatchInSeason = append(mostPlayerOfTheMatchInSeason,
			MostPlayerOfTheMatchInSeason{
				Season: season,
				Player: player,
				Wins:   wins})
	}

	file, _ := json.MarshalIndent(mostPlayerOfTheMatchInSeason, "", " ")
	_ = os.WriteFile("output/6-most-POTM-per-season.json", file, 0644)
}
