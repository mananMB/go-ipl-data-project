package src

import (
	"encoding/json"
	"fmt"
	"golang.org/x/exp/slices"
	"os"
	"sort"
	"strconv"
)

type BowlerEconomyRate struct {
	Bowler      string `json:"bowler"`
	EconomyRate string `json:"economy_rate"`
}

func CalculateTopTenEconomicalBowlers(matches []Match, deliveries []Delivery, year int) {
	var idsOfMatchesPlayedInYear = getIdOfMatchesPlayedInYear(matches, year)
	var bowlerStatsMap = make(map[string]map[string]float64)
	var bowlerEconomyRate []BowlerEconomyRate

	for _, delivery := range deliveries {
		if slices.Contains(idsOfMatchesPlayedInYear, delivery.MatchId) {
			if _, ok := bowlerStatsMap[delivery.Bowler]; !ok {
				bowlerStatsMap[delivery.Bowler] = make(map[string]float64)
			}
			var totalRuns, _ = strconv.Atoi(delivery.TotalRuns)
			bowlerStatsMap[delivery.Bowler]["runs"] += float64(totalRuns)
			bowlerStatsMap[delivery.Bowler]["balls"] += 1
		}
	}

	for bowler, stats := range bowlerStatsMap {
		economy := fmt.Sprintf("%.2f", stats["runs"]/(stats["balls"]/6))
		bowlerEconomyRate = append(bowlerEconomyRate, BowlerEconomyRate{Bowler: bowler, EconomyRate: economy})
	}

	sort.Slice(bowlerEconomyRate, func(bowlerA, bowlerB int) bool {
		economyA, _ := strconv.ParseFloat(bowlerEconomyRate[bowlerA].EconomyRate, 32)
		economyB, _ := strconv.ParseFloat(bowlerEconomyRate[bowlerB].EconomyRate, 32)
		return economyA < economyB
	})

	file, _ := json.MarshalIndent(bowlerEconomyRate[:10], "", " ")
	_ = os.WriteFile("output/4-top-ten-economical-bowlers.json", file, 0644)
}
