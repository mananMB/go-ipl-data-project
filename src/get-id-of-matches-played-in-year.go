package src

import "strconv"

func getIdOfMatchesPlayedInYear(matches []Match, year int) []string {
	var idsOfMatches []string

	for _, match := range matches {
		if match.Season == strconv.Itoa(year) {
			idsOfMatches = append(idsOfMatches, match.Id)
		}
	}

	return idsOfMatches
}
