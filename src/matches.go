package src

import (
	"encoding/csv"
	"io"
	"log"
	"os"
)

type Match struct {
	Id            string
	Season        string
	Winner        string
	TossWinner    string
	PlayerOfMatch string
}

const (
	id int = iota
	season
	city
	date
	team1
	team2
	tossWinner
	tossDecision
	result
	dlApplied
	winner
	winByRuns
	winByWickets
	playerOfMatch
	venue
	umpire1
	umpire2
	umpire3
)

func GetMatchesDataFromCSV() []Match {
	var matches []Match

	matchesFile, err := os.Open("./data/matches.csv")
	if err != nil {
		log.Fatal(err)
	}

	matchesReader := csv.NewReader(matchesFile)

	_, err = matchesReader.Read()
	if err != nil {
		log.Fatal(err)
	}

	for {
		row, err := matchesReader.Read()
		if err == io.EOF {
			break
		}
		if err != nil {
			log.Fatal(err)
		}
		matches = append(matches, Match{
			Season:        row[season],
			Winner:        row[winner],
			Id:            row[id],
			TossWinner:    row[tossWinner],
			PlayerOfMatch: row[playerOfMatch]})
	}

	return matches
}
